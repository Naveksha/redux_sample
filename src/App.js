import React from 'react';
import { FaArrowDown, FaArrowUp } from "react-icons/fa";
import { connect } from 'react-redux'
import { applySort,reset } from './redux/actions/tableData'
import './App.css';
import Quotes from './Quotes'

const Header = ['Name', 'Age', 'City']
const styles = {
  f1: {
    flex: 1
  },
  buttonContainer: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    marginTop: 30
  },
  container: {
    display: 'flex',
    flexDirection:'column',
    marginTop: 50
  },
  header: {
    display: 'flex',
    marginLeft:40,
  },
  df: {
    display:'flex'
  },
  reset: { marginLeft: 40, marginTop: 20 },
  heading: {
    width: '40%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 20
  }
}
class App extends React.Component
{
  constructor(props)
  { 
    super(props)
    this.state = {
      currentComponent:'table'
    }
  }
  setComponent = (component) =>
  {
    this.setState({currentComponent:component})
  }
  render() {
    let { items, sortApplied: { value = false, field = '', order = null } } = this.props
    let { currentComponent } =this.state
  return (
    <>
      <div style={styles.buttonContainer}>
        <button
          onClick={()=>{this.setComponent('table')}}
          style={{
            width: '30%',
            backgroundColor: currentComponent === 'table' ? 'grey' : 'darkgrey'  
          }}>Table</button>
        <button
          onClick={()=>{this.setComponent('quotes')}}
          style={{
            width: '30%',
            marginLeft:30,
            backgroundColor: currentComponent === 'quotes' ? 'grey' : 'darkgrey' 
          }}>Quotes</button>
      </div>
      {currentComponent === 'table' ? <>
        <h2 style={styles.heading}> Redux Synchronous Flow </h2>
        <div style={styles.container}>
          <div style={styles.header}>
            {Header.map((item, index) =>
            {
              let fieldName = item.toLowerCase()
              return (<div key={index} style={styles.f1}>{item}
                <FaArrowDown
                  color={value && field === fieldName && order === 'asc' ? "blue" : "black"}
                  onClick={() => this.props.sortData(fieldName, 'asc')} />
                <FaArrowUp
                  color={value && field === fieldName && order === 'desc' ? "blue" : "black"}
                  onClick={() => this.props.sortData(fieldName, 'desc')} />
              </div>)
            }
            )}
          </div>
          <ul style={{
          }}>

            {items.map(item =>
            {
              let { name = '', city = '', age = '', id = '' } = item
              return (
                <li style={styles.df}
                  key={id}
                >
                  <div style={styles.f1}>{name}</div>
                  <div style={styles.f1}>{age}</div>
                  <div style={styles.f1}>{city}</div>
                </li>)
            })}
          </ul>
        </div>
        <button style={styles.reset} onClick={() => this.props.reset()}>
          Reset
        </button>
      </>: <Quotes />}
      
    </>
  );
}
}

function mapStateToProps(state)
{
  let { tableState }= state
  return {
    items: tableState.data,
    sortApplied: tableState.sortApplied
  }
}

function mapDispatchToProps(dispatch)
{ 
  return {
    sortData: (field, order) => dispatch(applySort({ field, order })),
    reset: () => dispatch(reset())
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);
