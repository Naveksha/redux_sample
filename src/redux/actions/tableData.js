import { APPLY_SORT, RESET } from "../constants";
export const reset = () =>
{
    return {
        type: RESET
    }
}
export const applySort = (payload) =>
{
    return {
        type: APPLY_SORT,
        payload
    }
}

