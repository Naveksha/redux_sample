import { QUOTE_FAILED, QUOTE_REQUESTED, QUOTE_RECEIVED } from '../constants'
import axios from 'axios'
export const getQuote = () =>
{
  return function(dispatch) {
    dispatch({
      type: QUOTE_REQUESTED,
    });
    
  axios.get("https://ron-swanson-quotes.herokuapp.com/v2/quotes")
    .then(response => dispatch({
        type: QUOTE_RECEIVED,
        payload: response.data
      }))
    .catch(error =>
    {
      dispatch({
        type: QUOTE_FAILED,
        payload: error.message
      })
    }
    );
  }
}