import { combineReducers } from 'redux'
import tableReducer from './tableReducer' 
import quotesReducer from './quotesReducer'

export default combineReducers({
  tableState: tableReducer,
  quotesState: quotesReducer
})


//As your app grows more complex you will want to split your reducing 
// function into seperate functions, each managing  