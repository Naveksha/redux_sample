import _ from "lodash"
import { RESET, APPLY_SORT} from "../constants"
const initialState = {
    data: [
      { id: '0', age:20,name: "Naveksha", city:"Kanpur" },
      { id: '1', age:28,name: "Rashi" , city:"Rhotak"},
      { id: '2', age:22,name: "Priya", city: "Karnal" },
      { id: '3', age:19,name: "Monika", city:"Hissar" },
      { id: '4', age:30,name: "Divyanka", city:"Lucknow" }
  ],
  sortApplied:{ value: false, field:'', order: null }
};
const tableReducer = (state = initialState, action) =>
{
    switch (action.type)
    {
      case APPLY_SORT:
        let { field = '', order = '' } = action.payload
        let sortedData = [...state.data]
        sortedData=_.orderBy(sortedData,[field],[order] )
        return {
          ...state, data: sortedData,
          sortApplied: { value: true, field, order }
        }
              
      case RESET:
        return { ...initialState }
        default:
            return state;
    }
};
export default tableReducer;