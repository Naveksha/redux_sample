import { QUOTE_FAILED, QUOTE_REQUESTED, QUOTE_RECEIVED } from '../constants'
const initialState = {
  data: [],
  status:""
};
const quoteReducer = (state = initialState, action) =>
{
    switch (action.type)
    {

      case QUOTE_REQUESTED:
        return { ...state, status: 'loading' }
      case QUOTE_RECEIVED:
        return { ...state, data: [...action.payload], status:'success' }
      case QUOTE_FAILED:
        return { ...state, status: 'failed', error:action.payload }
      default:
        
            return state;
    }
};
export default quoteReducer;