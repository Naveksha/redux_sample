export const QUOTE_REQUESTED = "QUOTE_REQUESTED";
export const QUOTE_RECEIVED = "QUOTE_RECEIVED";
export const QUOTE_FAILED = "QUOTE_FAILED";
export const RESET = "RESET";
export const APPLY_SORT = "APPLY_SORT";

