import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getQuote } from './redux/actions/quotes'
const styles = {
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    padding:20
  },
  heading: {
    width: '40%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 20
  }

}
export class Quotes extends Component
{
 
  componentDidMount()
  {
    this.props.getQuote()
  }
  render() {

    let { data=[],status='', error='' }= this.props
    return (
      <>
        <div style={styles.container}>
        <h2 style={styles.heading}> Redux Asynchronous Flow </h2>
        <h1> Quote of the day </h1>
        {status === 'loading' ? <h3>Loading...</h3> : status === 'failed' ?
            <h3 style={{ color: "red" }}> {error} </h3> : <h2 style={{color:'#2E86C1',textAlign:'center'}}> {data[0]} </h2>}
          </div>
      </>
      ); 
  }
}
function mapStateToProps(state)
{
  let { quotesState } = state
  return {
    data: quotesState.data,
    status: quotesState.status,
    error: quotesState.error
   }
}
function mapDispatchToProps(dispatch)
{
  return {
    getQuote: () => { dispatch( getQuote())}
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Quotes)
